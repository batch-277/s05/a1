-- Cutsomers from the Philippines
SELECT customerName FROM customers WHERE country = "Philippines";

--  costomers with  name "La Rochelle Gifts"
SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

-- product with product name "The Titanic"
SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

-- first and last name of employees with email "jfirrelli@classicmodelcars.com"
SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

-- name of customers with no state  (or NULL)
SELECT customerName FROM customers WHERE state IS NULL;

-- last and first, and email of employee with last name "Patterson"  and first name "Steve"
SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson " AND firstName = "Steve";

-- name, country, and credit limit of customer who are not from the USA and credit limits are > 3000
SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

-- customer numbers with comment that contains "DHL"
SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

-- product lines with its text description contains the phrase "state of the art"
SELECT * FROM productlines WHERE textDescription LIKE "%state of the art%";

-- customer countries without duplications
SELECT DISTINCT country FROM customers;

-- order status without duplications
SELECT DISTINCT status FROM orders;

-- customer names and county of customers from USA, France, or Canada
SELECT customerName, country FROM customers WHERE country = "USA" OR  country = "France" OR  country = "Canada";

-- first and last name of employees in Tokyo
SELECT firstName, lastName, officeCode FROM employees WHERE officeCode = 5;

-- "Leslie Thompson"-served customers
SELECT customerName FROM customers WHERE salesRepEmployeeNumber = 1166;

-- product names and customer name of products ordered by "Baane Mini Imports"
SELECT products.productName, customers.customerName FROM products JOIN orderdetails ON products.productCode = orderdetails.productCode JOIN orders ON orderdetails.orderNumber = orders.orderNumber JOIN customers ON orders.customerNumber = customers.customerNumber WHERE customers.customerNumber = 121;

-- employees' first names, employees' last names, customers' names, and offices' countries of transactions whose customers and offices are in the same country
SELECT employees.firstName, employees.lastName, customers.customerName, offices.country FROM customers JOIN employees ON  customers.salesRepEmployeeNumber = employees.employeeNumber JOIN offices ON  employees.officeCode = offices.officeCode;

-- product name and quantity in stock of products that belong to the product line "planes" with stock quantities less than 1000.
SELECT products.productName, products.quantityInStock FROM products WHERE products.productLine = "Planes" AND products.quantityInStock < 1000;

-- customer's name with a phone number containing "+81"
SELECT customers.customerName FROM customers WHERE customers.phone LIKE "%+81%";